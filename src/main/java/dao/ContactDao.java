package dao;

import model.Contact;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Classe d'accès aux données d'un contact
 * - Habituellement on s'en sert pour requeter une base de données
 * - Dans le cadre du TP on y stockera juste une liste de contact en mémoire
 */
public class ContactDao implements IContactDao {

    private List<Contact> contacts = new ArrayList<>();

    public void add(Contact contact) {
        contacts.add(contact);
    }

    public List<Contact> get() {
        return contacts;
    }

    public Optional<Contact> find(String name) {
        /*for(model.Contact contact: contacts){
            if(name.equalsIgnoreCase(contact.getName())){
                return Optional.of(contact);
            }
        }
        return Optional.empty();*/
        return contacts.stream()
                .filter(contact -> name.equalsIgnoreCase(contact.getName()))
                .findFirst();
    }


}
