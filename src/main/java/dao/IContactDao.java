package dao;

import model.Contact;

import java.util.List;
import java.util.Optional;

/**
 * Interface de DAO de la base de contact
 */
public interface IContactDao {

    /**
     * Ajoute un contact
     *
     * @param contact contact à ajouter
     */
    void add(Contact contact);

    /**
     * Liste l'ensemble des contacts
     *
     * @return liste des contacts
     */
    List<Contact> get();

    /**
     * Recherche un contact par son nom
     *
     * @param name nom du contact
     * @return contact trouvé ou vide
     */
    Optional<Contact> find(String name);
}
