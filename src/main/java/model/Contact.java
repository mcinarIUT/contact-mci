package model;

/**
 * Classe représentant un contact
 */
public class Contact {
    /**
     * Nom du contact
     */
    private String name;
    /**
     * Numéro de téléphone
     */
    private String phoneNumber;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
