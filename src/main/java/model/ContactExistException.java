package model;

public class ContactExistException extends Exception {
    public ContactExistException(String message) {
        super(message);
    }
}
