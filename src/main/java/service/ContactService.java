package service;

import dao.ContactDao;
import model.ContactExistException;
import dao.IContactDao;
import model.Contact;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Classe de service de contact
 * - On y trouve les contrôles a effectuer sur la donnée entrante
 * - On y trouve les règles métiers
 */
public class ContactService {


    public static final int NAME_MIN_CHAR = 3;
    public static final int NAME_MAX_CHAR = 20;
    public static final int MAX_AWAIT_TIME = 2;

    private IContactDao contactDao = new ContactDao();

    /**
     * Méthode permettant d'enregistrer un nouveau contact
     *
     * @param name        nom du contact, obligatoire et unique [3-40] caractères
     * @param phoneNumber numéro de téléphone, obligatoire et au format FR (+10 chiffres)
     */
    public Contact add(String name, String phoneNumber) throws ContactExistException {

        if (name == null) {
            throw new IllegalArgumentException("Le nom ne doit pas être null");
        }
        if (name.length() < NAME_MIN_CHAR || name.length() > NAME_MAX_CHAR) {
            throw new IllegalArgumentException("Le nom doit faire entre 3 et 20 caractères");
        }

        Optional<Contact> foundContact = contactDao.find(name);
        if (foundContact.isPresent()) {
            throw new ContactExistException("Votre répertoire contient déjà un contact avec ce nom");
        }

        Contact contact = new Contact();
        contact.setName(name);
        contact.setPhoneNumber(phoneNumber);

        contactDao.add(contact);

        return contact;
    }

    public List<Contact> listAll() throws ExecutionException, InterruptedException, TimeoutException {
        return Executors.newFixedThreadPool(1)
                .submit(contactDao::get)
                .get(MAX_AWAIT_TIME, TimeUnit.SECONDS);
    }
}
