package service;

import model.ContactExistException;
import dao.IContactDao;
import model.Contact;
import org.easymock.*;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import service.ContactService;

import java.util.ArrayList;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class ContactServiceMockTest extends EasyMockSupport {

    public static final String VALID_PHONE_NUMBER = "0606060606";
    @Rule
    public EasyMockRule rule = new EasyMockRule(this);

    @TestSubject
    private ContactService contactService = new ContactService();

    @Mock
    private IContactDao contactDao;

    @Test(expected = ContactExistException.class)
    public void shouldRaiseExceptionOnDuplicateName() throws ContactExistException {
        String name = "Arnaud";
        EasyMock.expect(contactDao.find(name)).andReturn(Optional.of(new Contact()));
        replayAll();
        contactService.add(name, VALID_PHONE_NUMBER);
    }


    @Test
    public void shouldSucceedOnAdd() throws ContactExistException {
        String name = "Arnaud";
        EasyMock.expect(contactDao.find(name)).andReturn(Optional.empty());
        contactDao.add(EasyMock.anyObject(Contact.class));
        EasyMock.expectLastCall();
        replayAll();
        contactService.add(name, VALID_PHONE_NUMBER);

    }

    @Test
    public void shouldSucceedOnAddWithCapture() throws ContactExistException {
        String name = "Arnaud";
        EasyMock.expect(contactDao.find(name)).andReturn(Optional.empty());
        Capture<Contact> capturedContact = EasyMock.newCapture();
        contactDao.add(EasyMock.capture(capturedContact));
        EasyMock.expectLastCall();
        replayAll();
        Contact contact = contactService.add(name, VALID_PHONE_NUMBER);

        Assert.assertNotNull("Le contact ne doit pas être null", contact);
        Assert.assertEquals(name, contact.getName());
        Assert.assertEquals(VALID_PHONE_NUMBER, contact.getPhoneNumber());

        contact = capturedContact.getValue();
        Assert.assertEquals(name, contact.getName());
        Assert.assertEquals(VALID_PHONE_NUMBER, contact.getPhoneNumber());
    }


    @Test(expected = TimeoutException.class)
    public void shouldRaiseExceptionOnTooLongSearch() throws ContactExistException, ExecutionException, InterruptedException, TimeoutException {
        EasyMock.expect(contactDao.get()).andAnswer(() -> {
            Thread.sleep(2100);
            return new ArrayList<>();
        });
        replayAll();
        contactService.listAll();
    }



}
