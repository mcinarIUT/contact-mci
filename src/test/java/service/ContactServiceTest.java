package service;

import model.ContactExistException;
import model.Contact;
import org.junit.Assert;
import org.junit.Test;
import service.ContactService;

/**
 * Classe de test du service de contact
 */
public class ContactServiceTest {

    private ContactService service = new ContactService();
    private static final String VALID_PHONE_NUMBER = "0654123121";

    @Test
    public void shouldWorkForThree() throws ContactExistException {
        Contact contact = service.add("abc", VALID_PHONE_NUMBER);
        Assert.assertNotNull("Le contact ne doit pas être null", contact);
        Assert.assertEquals("abc", contact.getName());
        Assert.assertEquals(VALID_PHONE_NUMBER, contact.getPhoneNumber());
    }

    @Test
    public void shouldWorkForTwenty() throws ContactExistException {
        Contact contact = service.add("abcggggggggggggggggg", VALID_PHONE_NUMBER);
        Assert.assertNotNull("Le contact ne doit pas être null", contact);
        Assert.assertEquals("abcggggggggggggggggg", contact.getName());
        Assert.assertEquals(VALID_PHONE_NUMBER, contact.getPhoneNumber());
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailForTwo() throws ContactExistException {
        service.add("ab", VALID_PHONE_NUMBER);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailForTwentyOne() throws ContactExistException {
        service.add("abcggggggggggggggggga", VALID_PHONE_NUMBER);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldFailForNull() throws ContactExistException {
        service.add(null, VALID_PHONE_NUMBER);
    }


    @Test(expected = ContactExistException.class)
    public void shouldFailForNonUnique() throws ContactExistException {
        Contact contact = service.add("Arnaud", VALID_PHONE_NUMBER);
        Assert.assertNotNull("Le contact ne doit pas être null", contact);
        service.add("Arnaud", VALID_PHONE_NUMBER);
    }
}
